<?php
namespace Deduplicator;
use Deduplicator\DeduplicatorException;
use Deduplicator\Crypto\Crypto;
use finfo;
use Exception;
use PDO;

/**
 * File deduplicator (core).
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class Deduplicator
{
	const KEY_LENGTH = 22;
	const ALGO_CHECKSUM = 'fnv164';
	const LENGTH_HASH2 = 20;
	const OFFSET_INT8 = '9223372036854775807';

	private $pathFolder;
	private $pathDeduplication;
	private $pathCollision;
	private $storageLink;
	private $storageBlock;
	private $storageHashList;
	private $pdo;
	private $tableStat;

	public function __get($name)
	{
		if ($name == 'pathFolder')
		{ return $this->pathFolder; }
	}

	public function __construct($folder_path, $block_size, PDO $pdo_object, $tables_prefix = '')
	{
		date_default_timezone_set('UTC');  //TODO: try to move that in LinkStorage
		$this->pdo = $pdo_object;
		$this->tableStat = $tables_prefix . 'Stat';

		$this->pathFolder = self::checkFolder($folder_path);
		$this->pathDeduplication = self::checkFolder($this->pathFolder . 'Deduplicated/');
		$this->pathCollision = self::checkFolder($this->pathFolder . 'Collisions/');

		$this->storageLink = new LinkStorage($this->pdo, $tables_prefix);
		$this->storageBlock = new BlockStorage($block_size, $this->pdo, $this->tableStat, $tables_prefix);
		$this->storageHashList = new HashListStorage(
			$this->pathDeduplication, Crypto::COMBOHASH_LENGTH, $this->pdo, $this->tableStat);
	}

	/**
	 * Put a new uploaded file in the waiting line to be deduplicated later.
	 * @param string $path Temp path of uploaded file
	 * @param string $name Uploaded filename
	 * @return string[] Link ID + code and Filename
	 */
	public function putFile($path, $name)
	{
		$size = filesize($path);

		if ($size < 1)
		{
			unlink($path);
			return array();
		}

		set_time_limit(300);  // Max response timeout for all browsers (5 min)
		$name = trim(preg_replace('/[\x00-\x1F\x7F\xA0\\/\|\*\?\:\<\>\"]/u', '', $name), '. ');
		$file_key = hash_init(self::ALGO_CHECKSUM);
		$sha512 = hash_init('sha512');
		$file = fopen($path, 'rb');
		$block = fread($file, $this->storageBlock->blockSize);

		while ($block !== false && $block !== '')
		{
			hash_update($sha512, $block);
			hash_update($file_key, Crypto::comboHash($block, true));

			$block = fread($file, $this->storageBlock->blockSize);
		}

		fclose($file);
		$file_key = hash_final($file_key, true);
		$sha512 = hash_final($sha512, true);
		$file_id = Crypto::comboHash($sha512 . $file_key, true);
		$file_id_hex = bin2hex($file_id);
		$new_path = $this->pathFolder . $file_id_hex;

		if (is_file($new_path) || $this->storageHashList->exist($file_id_hex))
		{ unlink($path); }  // File already waiting or deduplicated
		else
		{ move_uploaded_file($path, $new_path); }  // ready to store blocks

		$link_key = Crypto::randomAlphaNum(self::KEY_LENGTH);
		$link_id = $this->storageLink->create($link_key, $name, $file_key, $file_id, $sha512);

		$this->pdo->exec(
			"update $this->tableStat set Counter1 = Counter1 +1, Counter2 = Counter2 + $size where ID=1");

		return ['ID' => $link_id, 'Code' => $link_key, 'Name' => $name];
	}

	/**
	 * Get all informations about a link and its file related.
	 * @param string $id
	 * @param string $key
	 * @return array Name,Upload,Access,Remove,Download,FileID,Checksum,SHA512,SHA256,SHA1,MD5,Size
	 */
	public function getFileInfos($id, $key)
	{
		$infos = $this->storageLink->find($id, $key);

		if (empty($infos))
		{ return array(); }

		$file_id = bin2hex($infos['FileID']);
		$hash_list = $this->storageHashList->open($file_id, $infos['Checksum']);  // Checksum = File key
		$path = $this->pathFolder . $file_id;

		if ($hash_list != null)
		{
			$infos['Size'] = ($this->storageHashList->countBlock($file_id) -1) * $this->storageBlock->blockSize;
			$infos['Size'] += strlen($this->storageBlock->find($hash_list->readEnd()));
		}
		else if (is_file($path))
		{
			$infos['Size'] = filesize($path);
		}

		if ($infos['Remove'] < 0)
		{
			$this->pdo->exec('update ' . $this->tableStat
				. ' set Counter1 = Counter1 -1, Counter2 = Counter2 - ' . $infos['Size'] . ' where ID=1');

			return array();
		}

		if ($infos['Name'] == '')
		{ $infos['Name'] = 'NO NAME'; }

		return $infos;
	}

	/**
	 * Download a file from a link.
	 * @param string $link
	 * @param string $code
	 * @throws Exception
	 */
	public function download($link, $code)
	{
		$infos = $this->getFileInfos($link, $code);

		if (empty($infos))
		{ return false; }

		set_time_limit(7200);  // 2 hours (max download time)
		// Examples of download speed  ------->  Filesize downloadable
		// old modem:  56 Kbit/s ->   7 KBytes x 7200 sec =  50 MB max
		// slow ADSL: 512 Kbit/s ->  64 KBytes x 7200 sec = 450 MB max
		// avg. ADSL:   8 Mbit/s ->   1 MBytes x 7200 sec =   7 GB max
		// avg mobile: 12 Mbit/s -> 1.5 MBytes x 7200 sec =  10 GB max
		// avg ADSL2+: 16 Mbit/s ->   2 MBytes x 7200 sec =  14 GB max

		$this->storageLink->incrementDownloadCounter($link);
		$infos['FileID'] = bin2hex($infos['FileID']);
		$hash_list = $this->storageHashList->open($infos['FileID'], $infos['Checksum']);

		if ($hash_list == null)  // Download file not deduplicated...
		{ $this->downloadFile($infos['FileID'], $infos['Name'], $infos['SHA512'], $infos['Size']); }

		// Download deduplicated file...
		$hash = $hash_list->read();
		$block = $this->storageBlock->find($hash);

		$finfo = new finfo(FILEINFO_MIME);  // or FILEINFO_MIME_TYPE
		self::downloadHeader($infos['Name'], $infos['Size'], $finfo->buffer($block));
		echo $block;

		while (($hash = $hash_list->read()) !== '')
		{
			$block = $this->storageBlock->find($hash);

			if ($block === '')
			{ throw new Exception('Unknown block: ' . bin2hex($hash)); }

			echo $block;
		}
	}

	/**
	 * Direct download of a file (not deduplicated).
	 * @param string $id (hexadecimal)
	 * @param string $name
	 * @param string $sha512 (binary)
	 * @param int $size
	 * @throws Exception
	 */
	private function downloadFile($id, $name, $sha512, $size)
	{
		$path = $this->pathCollision . $id;

		if (!is_file($path))
		{ $path = $this->pathFolder . $id; }

		if (!is_file($path))
		{ throw new Exception('File not found: ' . $id); }

		if (hash_file('sha512', $path, true) != $sha512)
		{ throw new Exception('Corrupted file: ' . $id); }

		$finfo = new finfo(FILEINFO_MIME);  // or FILEINFO_MIME_TYPE
		self::downloadHeader($name, $size, $finfo->file($path));
		readfile($path);
	}

	/**
	 * Send HTTP header to start a downloading.
	 * @param string $filename
	 * @param int $filesize
	 * @param string $mime_type
	 */
	static function downloadHeader($filename, $filesize, $mime_type)
	{
		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename="' . $filename . '"');
		header('Content-Length: ' . $filesize);

		if ($mime_type != '' && strpos($mime_type, 'x-empty') === false)
		{ header('Content-Type: ' . $mime_type); }

		//if (ob_get_length()) { ob_end_flush(); }
	}

	/**
	 * Deduplicate a pending file.
	 * @param string $path
	 */
	public function storeFile($path)
	{
		$sha512 = hash_init('sha512');
		$sha256 = hash_init('sha256');
		$sha1 = hash_init('sha1');
		$md5 = hash_init('md5');
		$checksum = hash_init(self::ALGO_CHECKSUM);

		$file = fopen($path, 'rb');
		$block = fread($file, $this->storageBlock->blockSize);

		while ($block !== false && $block !== '')
		{
			hash_update($sha512, $block);
			hash_update($sha256, $block);
			hash_update($sha1, $block);
			hash_update($md5, $block);
			hash_update($checksum, Crypto::comboHash($block, true));

			$block = fread($file, $this->storageBlock->blockSize);
		}

		$sha512 = hash_final($sha512, true);
		$sha256 = hash_final($sha256, true);
		$sha1 = hash_final($sha1, true);
		$md5 = hash_final($md5, true);
		$checksum = hash_final($checksum, true);

		$file_id = Crypto::comboHash($sha512 . $checksum, true);
		$this->storageLink->updateFile($file_id, $checksum, $sha256, $sha1, $md5);
		$hash_list = $this->storageHashList->create(bin2hex($file_id), $checksum);  // Key = checksum of stream

		if ($hash_list == null)
		{ throw new Exception('File already deduplicated: ' . $path); }

		// Block deduplication
		rewind($file);

		try
		{
			$block = fread($file, $this->storageBlock->blockSize);

			while ($block !== false && $block !== '')
			{
				$hash_list->write($this->storageBlock->store($block));
				$block = fread($file, $this->storageBlock->blockSize);
			}
		}
		catch (DeduplicatorException $ex)
		{
			fclose($file);
			$hash_list->drop();

			while (!rename($path, $this->pathCollision . basename($path)))
			{ sleep(1); }

			$ex->addDetail('upload', $path);
			throw $ex;
		}

		fclose($file);
		$hash_list->store();

		while (!unlink($path))
		{ sleep(1); }
	}

	public function partialFileExist()
	{
		return $this->storageHashList->partialHashExist();
	}

	public function totalPendingFiles()
	{
		$counters = ['File' => 0, 'Size' => 0];
		$path_folder = $this->pathFolder;

		foreach (scandir($path_folder, SCANDIR_SORT_NONE) as $filename)
		{
			$filepath = $path_folder . $filename;

			if (is_file($filepath))
			{
				$counters['Size'] += filesize($filepath);
				++$counters['File'];
			}
		}

		return $counters;
	}

	/**
	 * Return statictics about specific data.
	 * @param int $id
	 * @return int[] Counter1, Counter2 (bigint)
	 */
	public function getStats($id)
	{
		$sql = 'select Counter1 + ' . self::OFFSET_INT8 . ' as Counter1, Counter2 + ' . self::OFFSET_INT8
			. ' as Counter2 from ' . $this->tableStat . ' where ID=' . $id;

		$stmt = $this->pdo->query($sql);

		if ($stmt == false)  // or null
		{
			$this->initDatabase();
			$stmt = $this->pdo->query($sql);
		}

		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Copy a link.
	 * @param string $name
	 * @param string $link
	 * @param string $code
	 * @return string[] ID, Code
	 */
	public function copy($name, $link, $code)
	{
		$infos = $this->getFileInfos($link, $code);

		if (!isset($infos['Size']))
		{ return ''; }  // Bad key

		$name = trim(preg_replace('/[\x00-\x1F\x7F\xA0\\/\|\*\?\:\<\>\"]/u', '', $name), '. ');
		$key = Crypto::randomAlphaNum(self::KEY_LENGTH);
		$id = $this->storageLink->create($key, $name, $infos['Checksum'], $infos['FileID']);

		$this->pdo->exec('update ' . $this->tableStat
			. ' set Counter1 = Counter1 +1, Counter2 = Counter2 + ' . $infos['Size'] . ' where ID=1');

		return ['ID' => $id, 'Code' => $key];
	}

	/**
	 * Drop a link.
	 * @param string $link
	 * @param string $code
	 * @param int $second
	 * @return boolean
	 */
	public function delete($link, $code, $second)
	{
		$infos = $this->getFileInfos($link, $code);

		if (isset($infos['Size']) && $this->storageLink->drop($link, $second))
		{
			$this->pdo->exec('update ' . $this->tableStat
				. ' set Counter1 = Counter1 -1, Counter2 = Counter2 - ' . $infos['Size'] . ' where ID=1');

			return true;
		}

		return false;  // Bad key
	}

	/**
	 * Create "Stat" table in the database and insert needed lines.
	 */
	private function initDatabase()
	{
		$min = '-' . self::OFFSET_INT8;
		$bigint = 'bigint';

		if ($this->pdo->getAttribute(PDO::ATTR_DRIVER_NAME) == 'oci')
		{ $bigint = 'number(19)'; }

		$this->pdo->exec("create table $this->tableStat ("
			. "ID smallint primary key,"
			. "Description varchar(255) not null,"
			. "Counter1 $bigint,"
			. "Counter2 $bigint)");

		$this->pdo->exec("insert into $this->tableStat values (1, 'Links (total & filesize)', $min , $min )");

		$this->pdo->exec(
			"insert into $this->tableStat values (2, 'Files (hash list & hashes)', $min , $min )");

		$this->pdo->exec(
			"insert into $this->tableStat values (3, 'Encrypted blocks (total & size)', $min , $min )");

		$this->pdo->exec(
			"insert into $this->tableStat values (4, 'Compressed blocks (total & size)', $min , $min )");
	}

	static function checkPath($path)
	{
		$last_char = substr($path, -1);

		if ($last_char != '/' && $last_char != '\\')
		{ return $path . DIRECTORY_SEPARATOR; }

		return $path;
	}

	static function checkFolder($path)
	{
		if (!is_dir($path))  // or not exists
		{
			if (!mkdir($path, 0777, true))
			{ throw new Exception('Unable to create folder ' . $path); }
		}

		return self::checkPath($path);
	}
}