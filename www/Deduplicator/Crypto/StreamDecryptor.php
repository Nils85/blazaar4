<?php
namespace Deduplicator\Crypto;

/**
 * Decrypt a stream in counter mode.
 * @uses OpenSSL
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class StreamDecryptor
{
	private $key;
	private $counter;

	function __get($name)
	{
		if ($name == 'counter')
		{ return $this->counter; }
	}

	function __construct($key, $counter = 0)
	{
		$this->counter = $counter;
		$this->key = hash(Crypto::KEY_ALGO, $key, true);
	}

	public function decrypt($data)
	{
		return openssl_decrypt($data, Crypto::CIPHER, $this->key, OPENSSL_RAW_DATA,
			hash(Crypto::IV_ALGO, $this->counter++, true));
	}
}