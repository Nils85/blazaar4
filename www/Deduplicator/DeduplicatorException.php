<?php
namespace Deduplicator;
use Exception;

/**
 * Exception for deduplication error and warning.
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class DeduplicatorException extends Exception
{
	/**
	 * Constructor.
	 * @param string $type Collision, corruption, etc...
	 * @param string $item_id Item ID (binary, hex, text...)
	 */
	public function __construct($type, $item_id)
	{
		parent::__construct();
		$this->message = $type . ' ' . self::printableVar($item_id);
		$this->code = E_USER_ERROR;

		if (stripos($type, 'collision') !== false)
		{ $this->code = E_USER_WARNING; }
	}

	/**
	 * Add an informtion about this exception.
	 * @param string $information
	 * @param string $item_id
	 */
	public function addDetail($information, $item_id)
	{
		$this->message .= ' ' . $information . ' ' . self::printableVar($item_id);
	}

	static function printableVar($item_id)
	{
		if ($item_id === null)
		{ return 'NULL'; }

		if (is_bool($item_id))
		{ return $item_id ? 'TRUE' : 'FALSE'; }

		if (!is_string($item_id))
		{ return print_r($item_id, true); }

		if ($item_id === '')
		{ return 'EMPTY'; }

		if (!ctype_print($item_id))
		{ return bin2hex($item_id); }

		if (strlen($item_id) > 131)
		{ return '...' . substr($item_id, -128); }

		return $item_id;
	}
}