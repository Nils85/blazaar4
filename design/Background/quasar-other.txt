GIMP 2.10

Imported from "quasar-other.jpg"

Image > Scale Image
Width: 1366
Height: 841
Interpolation: Cubic

Filter > Blur > Gaussian Blur
Size X: 70
Size Y: 70
Filter: Auto
Abyss policy: Clamp
Clip to the input extent

Export Image as "bg3.png"
No interlacing (Adam7)
Don't save background color, gamma, layer offset, resolution, time...
No comment
Automatic pixelformat
Compression level: 9
Don't save Exif, XMP, IPTC, thumbnail or color profile