<?php
namespace Deduplicator;
use Deduplicator\DeduplicatorException;
use Deduplicator\Crypto\Crypto;
use PDO;

/**
 * Storage engine for data blocks.
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class BlockStorage
{
	const ID_HALF_LENGTH = 8;  // bytes
	const ALGO ='CAST5-CFB';
	const IV = ' Blazar ';  // 8 bytes

	private $pdo;
	private $tableShortKey;
	private $tableLongKey;
	private $tableSmallBlock;
	private $tableStat;
	private $blockSize;
	private $blockIndex;

	public function __get($name)
	{
		if ($name == 'blockSize')
		{ return $this->blockSize; }
	}

	function __construct($block_size, PDO &$pdo_instance, $table_stat, $tables_prefix = '')
	{
		$this->pdo = $pdo_instance;
		$this->tableShortKey = $tables_prefix . 'Block_';
		$this->tableLongKey = $tables_prefix . 'Block';
		$this->tableSmallBlock = $tables_prefix . 'BlockSmall';
		$this->tableStat = $table_stat;
		$this->blockSize = $block_size;
		$this->blockIndex = $block_size -1;
	}

	public function store($data)
	{
		$key = Crypto::comboHash($data, true);
		$original_size = strlen($data);
		$scrambled_data = gzdeflate($data, 9);
		$compressed_size = strlen($scrambled_data);

		if ($compressed_size < $original_size)
		{
			$this->pdo->exec('update ' . $this->tableStat
				. ' set Counter1 = Counter1 +1, Counter2 = Counter2 + ' . $compressed_size . ' where ID=4');
		}
		else
		{
			$scrambled_data = openssl_encrypt($data, self::ALGO, $key, OPENSSL_RAW_DATA, self::IV);  // jamming
			$this->pdo->exec('update ' . $this->tableStat
				. ' set Counter1 = Counter1 +1, Counter2 = Counter2 + ' . $original_size . ' where ID=3');
		}

		if ($original_size < $this->blockSize)
		{
			if ($this->storeSmallBlock($key, $scrambled_data))
			{ return $key; }

			if ($this->find($key) === $data)
			{ return $key; }

			throw new DeduplicatorException('Block collision', $key);
		}

		if($this->storeHalfKey(substr($key, 0, self::ID_HALF_LENGTH), $scrambled_data))
		{ return $key; }

		if ($this->find($key) === $data)
		{ return $key; }

		if ($this->storeFullKey($key, $scrambled_data))
		{ return $key; }

		throw new DeduplicatorException('Block collision', $key);
	}

	private function storeHalfKey($key, $data)
	{
		static $prepared_statements = array();

		if (empty($prepared_statements))
		{
			$i = 0;

			while ($i < 256)
			{
				$suffix = str_pad(dechex($i), 2, '0', STR_PAD_LEFT);

				$prepared_statement = $this->pdo->prepare(
					'insert into ' . $this->tableShortKey . $suffix . '(ID,Data) values (?,?)');

				if ($prepared_statement == false)  // or null
				{
					$this->initDatabase();
					continue;  // retry to prepare statement
				}

				$prepared_statements[hex2bin($suffix)] = $prepared_statement;
				++$i;
			}
		}

		$prepared_statement = $prepared_statements[$key[0]];

		// Don't use bindParam! it can cause segmentation fault with null bytes
		$prepared_statement->bindValue(1, substr($key,1), PDO::PARAM_LOB);
		$prepared_statement->bindValue(2, $data, PDO::PARAM_LOB);

		return $prepared_statement->execute();
	}

	private function storeFullKey($key, $data)
	{
		static $prepared_statement = false;

		if (!$prepared_statement)
		{ $prepared_statement = $this->pdo->prepare("insert into $this->tableLongKey (ID,Data) values (?,?)"); }

		$prepared_statement->bindValue(1, $key, PDO::PARAM_LOB);
		$prepared_statement->bindValue(2, $data, PDO::PARAM_LOB);

		return $prepared_statement->execute();
	}

	private function storeSmallBlock($key, $data)
	{
		$prepared_statement = $this->pdo->prepare("insert into $this->tableSmallBlock (ID,Data) values (?,?)");
		$prepared_statement->bindValue(1, $key, PDO::PARAM_LOB);
		$prepared_statement->bindValue(2, $data, PDO::PARAM_LOB);

		return $prepared_statement->execute();
	}

	public function find($key)
	{
		$possible_corruption = false;
		$data = $this->findHalfKey(substr($key, 0, self::ID_HALF_LENGTH));

		if ($data !== '')
		{
			if (!isset($data[$this->blockIndex]))  // strlen($data) != $this->blockSize
			{ $data = gzinflate($data); }
			else
			{ $data = openssl_decrypt($data, self::ALGO, $key, OPENSSL_RAW_DATA, self::IV); }

			if (Crypto::comboHash($data, true) === $key)
			{ return $data; }

			$possible_corruption = true;
		}

		$data = $this->findFullKey($key);

		if ($data !== '')
		{
			if (!isset($data[$this->blockIndex]))  // strlen($data) != $this->blockSize
			{ $data = gzinflate($data); }
			else
			{ $data = openssl_decrypt($data, self::ALGO, $key, OPENSSL_RAW_DATA, self::IV); }

			if (Crypto::comboHash($data, true) === $key)
			{ return $data; }

			throw new DeduplicatorException('Corrupted block', $key);
		}

		$data = $this->findSmallBlock($key);

		if ($data === '')
		{
			if ($possible_corruption)
			{ throw new DeduplicatorException('Corrupted block', $key); }

			trigger_error('Block not found ' . bin2hex($key));
			return '';
		}

		$descrambled_data = openssl_decrypt($data, self::ALGO, $key, OPENSSL_RAW_DATA, self::IV);

		if (Crypto::comboHash($descrambled_data, true) === $key)
		{ return $descrambled_data; }

		$data = gzinflate($data);

		if (Crypto::comboHash($data, true) === $key)
		{ return $data; }

		throw new DeduplicatorException('Corrupted block', $key);
	}

	private function findHalfKey($key)
	{
		static $prepared_statements = array();

		if (empty($prepared_statements))
		{
			for ($i=0; $i < 256; ++$i)
			{
				$suffix = str_pad(dechex($i), 2, '0', STR_PAD_LEFT);

				$prepared_statement = $this->pdo->prepare(
					'select Data from ' . $this->tableShortKey . $suffix . ' where ID=?');

				$prepared_statements[hex2bin($suffix)] = $prepared_statement;
			}
		}

		$prepared_statement = $prepared_statements[$key[0]];
		$prepared_statement->bindValue(1, substr($key, 1), PDO::PARAM_LOB);
		$prepared_statement->execute();
		$rows = $prepared_statement->fetchAll(PDO::FETCH_COLUMN, 0);

		if (empty($rows))
		{ return ''; }

		return $rows[0];
	}

	private function findFullKey($key)
	{
		static $prepared_statement = false;

		if (!$prepared_statement)
		{ $prepared_statement = $this->pdo->prepare("select Data from $this->tableLongKey where ID=?"); }

		$prepared_statement->bindValue(1, $key, PDO::PARAM_LOB);
		$prepared_statement->execute();
		$rows = $prepared_statement->fetchAll(PDO::FETCH_COLUMN, 0);

		if (empty($rows))
		{ return ''; }

		return $rows[0];
	}

	private function findSmallBlock($key)
	{
		$prepared_statement = $this->pdo->prepare("select Data from $this->tableSmallBlock where ID=?");
		$prepared_statement->bindValue(1, $key, PDO::PARAM_LOB);
		$prepared_statement->execute();
		$rows = $prepared_statement->fetchAll(PDO::FETCH_COLUMN, 0);

		if (empty($rows))
		{ return ''; }

		return $rows[0];
	}

	/**
	 * Create tables for blocks storage in the database.
	 */
	private function initDatabase()
	{
		$type_id = $type_bigid = $type_block = $type_small_block = 'blob';

		switch ($this->pdo->getAttribute(PDO::ATTR_DRIVER_NAME))
		{
			case 'pgsql':
				$type_id = $type_bigid = $type_block = $type_small_block = 'bytea';
				break;

			case 'mysql':
			case 'sqlsrv':
				$type_id = 'binary(' . (self::ID_HALF_LENGTH -1) . ')';
				$type_bigid = 'binary(' . strlen(Crypto::comboHash('',true)) . ')';
				$type_block = 'varbinary(' . $this->blockSize . ')';
				$type_small_block = 'varbinary(' . $this->blockIndex . ')';
				break;
		}

		for ($i=0; $i < 256; ++$i)
		{
			$this->pdo->exec(
				'create table ' . $this->tableShortKey . str_pad(dechex($i), 2, '0', STR_PAD_LEFT)
				. '(ID ' . $type_id . ' primary key, Data ' . $type_block . ' not null)');
		}

		$this->pdo->exec(
			"create table $this->tableLongKey (ID $type_bigid primary key, Data $type_block not null)");

		$this->pdo->exec(
			"create table $this->tableSmallBlock (ID $type_bigid primary key, Data $type_small_block not null)");
	}
}