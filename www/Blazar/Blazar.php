<?php
namespace Blazar;
use Deduplicator\Deduplicator;
use PDO;

/**
 * Static class for common functions of the web app.
 * @author Vincent Boursier <vincent.boursier@gmail.com>
 */
class Blazar
{
	/**
	 * Build a Deduplicator.
	 * @return Deduplicator
	 */
	static function getDeduplicator()
	{
		$files_path = Config::PATH_FILES;

		if ($files_path[0] != '/' && $files_path[1] != ':')
		{
			$directory = dirname(__DIR__);

			while (substr($files_path, 0, 2) == '..')
			{
				$directory = dirname($directory);
				$files_path = substr($files_path, 3);
			}

			$files_path = $directory . '/' . $files_path;
		}

		$pdo = new PDO(Config::DB_SOURCE, Config::DB_USERNAME, Config::DB_PASSWORD, [
			PDO::ATTR_PERSISTENT => true,
			PDO::ATTR_EMULATE_PREPARES => false,
			PDO::ATTR_STRINGIFY_FETCHES => false]);

		return new Deduplicator($files_path, Config::BLOCK_SIZE, $pdo, Config::DB_TABLES_PREFIX);
	}

	/**
	 * Detect the maximum size for an upload according to the php.ini
	 * @return string Max filesize
	 */
	static function getMaxUploadSize()
	{
		$max_size = ini_get('upload_max_filesize');
		$post_max_size = ini_get('post_max_size');
		$post_max_unit = substr($post_max_size, -1);
		$max_size_unit = substr($max_size, -1);

		if ($max_size_unit === $post_max_unit)
		{
			if ((int)$post_max_size < (int)$max_size)
			{ $max_size = $post_max_size; }
		}
		elseif ($max_size_unit == 'G' && $post_max_unit == 'M')
		{
			$max_size = $post_max_size;
			$max_size_unit = $post_max_unit;
		}

		if ($max_size_unit == 'G' && $max_size != '1G')
		{
			if (PHP_INT_SIZE == 4)  // 32bit CPU
			{ $max_size = '2G'; }

			$max_input_time = ini_get('max_input_time');

			if ($max_input_time == -1)
			{ $max_input_time = ini_get('max_execution_time'); }

			if ($max_input_time > 300)
			{ $max_input_time = 300; }

			if ($max_input_time > 0)
			{
				$max_input_size = (int)ceil($max_input_time / 20);  // max_input_time / 60sec * 3G

				if ($max_input_size < (int)$max_size)
				{ $max_size = $max_input_size . 'G'; }
			}
		}

		return $max_size;
	}

	static function getBackground()
	{
		$bg = date('n');  // Default month number

		if (filter_has_var(INPUT_GET, 'bg'))
		{
			$bg = filter_input(INPUT_GET, 'bg');
			setcookie('bg', $bg, 2147483647, '/');  // Never expire
		}
		elseif (filter_has_var(INPUT_COOKIE, 'bg'))
		{
			$bg = filter_input(INPUT_COOKIE, 'bg');
		}

		switch ($bg)
		{
			case '10':
			case '7':
			case '4':
			case '1': return Config::BACKGROUND1;
			case '11':
			case '8':
			case '5':
			case '2': return Config::BACKGROUND2;
		}

		return Config::BACKGROUND3;  // case 3, 6 and 12
	}
}