<?php
spl_autoload_register(function($class_name) {
	require '../' . str_replace('\\', '/', $class_name) . '.php';
});

$deduplicator = Blazar\Blazar::getDeduplicator();
$pending_files = $deduplicator->totalPendingFiles();

$stats = $deduplicator->getStats(1);
$stats_files = $deduplicator->getStats(2);
$stats_encrypted_blocks = $deduplicator->getStats(3);
$stats_compressed_blocks = $deduplicator->getStats(4);

$view_head = [
	0 => Blazar\Blazar::getBackground(),
	1 => $stats['Counter1'],
	2 => $stats['Counter2']];

$view = [
	0 => $stats_files['Counter1'],
	1 => $stats_compressed_blocks['Counter1'] + $stats_encrypted_blocks['Counter1'],
	2 => $stats_files['Counter2'],
	3 => Blazar\Config::BLOCK_SIZE,
	4 => $stats_compressed_blocks['Counter2'] + $stats_encrypted_blocks['Counter2'],
	5 => $pending_files['File'],
	6 => $pending_files['Size'],
	7 => $stats['Counter1']];

$language = Blazar\Config::DEFAULT_LANGUAGE;
include '../Translations/translate.php';
include '../Templates/head.php';
include 'view.php';
include '../Templates/foot.php';