<div class="Z">
	<span class="V"><?php echo TRANS[51] ?></span>
	<br/><br/>
	<?php echo TRANS[52] ?>
	<br/><br/>
	<?php echo TRANS[53] ?>
	<br/><br/>
	<?php
		$translations = [
			TRANS[54] => TRANS[55],
			TRANS[56] => sprintf(TRANS[57], '<a href="/delete">"delete"</a>'),
			TRANS[58] => sprintf(TRANS[59], Blazar\Blazar::getMaxUploadSize()),
			TRANS[60] => TRANS[61],
			TRANS[62] => TRANS[63],
			TRANS[64] => TRANS[65],
			TRANS[66] => Blazar\Config::DATACENTER_LOCATION,
			TRANS[67] => TRANS[68],
			TRANS[69] => Blazar\Config::EMAIL];

		$i = 0;

		foreach ($translations as $question => $answer)
		{ echo '<a href="#answer', ++$i, '">', $question, '</a><br/>'; }
	?>
	<a name="answer1"></a>
</div>
<?php
	$i = 1;

	foreach ($translations as $question => $answer)
	{
		echo '<div class="Z"><span class="V">', $question,
			'</span><br/><br/>', $answer, '<br/><a name="answer', ++$i, '"></a></div>';
	}
?>