<?php
spl_autoload_register(function($class_name) {
	require '../' . str_replace('\\', '/', $class_name) . '.php';
});

$deduplicator = Blazar\Blazar::getDeduplicator();
$view_page = 'view delete.php';

if (filter_has_var(INPUT_POST, 'link'))
{
	$code = filter_input(INPUT_POST, 'code');
	$link = filter_input(INPUT_POST, 'link');
	$position = strpos($link, '=');

	if ($position)
	{ $link = substr($link, $position +1); }

	$file = $deduplicator->getFileInfos($link, $code);

	if (isset($file['Size']))
	{
		$view_page = 'view confirm.php';
		$view = [
			0 => $link,
			1 => $code];

		$view_info = [
			0 => htmlspecialchars($file['Name']),
			1 => bin2hex($file['SHA512']),
			2 => $file['Download'],
			3 => $file['Upload'],
			4 => $file['Access'],
			5 => $file['Size'],
			6 => $file['Remove']];

		if (isset($file['SHA256']))
		{
			$view_info[-1] = bin2hex($file['SHA256']);
			$view_info[-2] = bin2hex($file['SHA1']);
			$view_info[-3] = bin2hex($file['MD5']);
		}
	}
}
elseif (filter_has_var(INPUT_POST, 'drop'))
{
	if ($deduplicator->delete(
		filter_input(INPUT_POST, 'drop'),
		filter_input(INPUT_POST, 'code'), Blazar\Config::LINK_DROP_DELAY * 3600))
	{
		$view_page = 'view message.php';
	}
}

$stats = $deduplicator->getStats(1);
$view_head = [
	0 => Blazar\Blazar::getBackground(),
	1 => $stats['Counter1'],
	2 => $stats['Counter2']];

$language = Blazar\Config::DEFAULT_LANGUAGE;
include '../Translations/translate.php';
include '../Templates/head.php';
include $view_page;
include '../Templates/foot.php';