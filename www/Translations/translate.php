<?php
if(!isset($language))
{ $language = 'en'; }

$languages = array();

foreach (scandir(__DIR__ . '/trans') as $name)
{
	if (isset($name[9]))  // strlen($name) > 9
	{ $languages[substr($name, 0, 2)] = substr($name, 3, -4); }
}

if(!isset($languages[$language]))
{
	// PHP7: $language = array_key_first($languages);
	foreach ($languages as $key => $value)
	{
		$language = $key;
		break;
	}
}

if (filter_has_var(INPUT_GET, 'lang'))
{
	$lang = filter_input(INPUT_GET, 'lang');

	if(isset($languages[$lang]))
	{
		setcookie('lang', $lang, 2147483647, '/');
		$language = $lang;
	}
}
elseif (filter_has_var(INPUT_COOKIE, 'lang'))
{
	$lang = filter_input(INPUT_COOKIE, 'lang');

	if(isset($languages[$lang]))
	{ $language = $lang; }
}
elseif (filter_has_var(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE'))
{
	foreach (explode(',', filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE')) as $accept_language)
	{
		$lang = substr($accept_language, 0, 2);

		if (isset($languages[$lang]))
		{
			$language = $lang;
			break;
		}
	}
}

require 'trans/' . $language . ' ' . $languages[$language] . '.php';

/**
 * format a number according to language.
 * @param int $number
 * @param int $decimal
 * @return string
 */
function translate_num($number, $decimal = 0)
{
	return number_format($number, $decimal, TRANS[-2], TRANS[-1]);
}

/**
 * Format a filesize according to language.
 * @param int $filesize
 * @return string
 */
function translate_size($filesize)
{
	if ($filesize > 1073741823)
	{ return translate_num($filesize / 1073741824) . ' ' . TRANS[-8]; }  // GB

	if ($filesize > 1048575)
	{ return translate_num($filesize / 1048576) . ' ' . TRANS[-7]; }  // MB

	if ($filesize > 1023)
	{ return translate_num($filesize / 1024) . ' ' . TRANS[-6]; }  // KB

	return $filesize . ' ' . TRANS[-5];  // Bytes
}

/**
 * Format a time interval according to language.
 * @param int $timestamp
 * @return string
 */
function translate_interval($timestamp)
{
	$interval = abs(time() - $timestamp);

	if ($interval < 90)
	{ return $interval . ' ' . TRANS[-3]; }  // seconds

	$interval = $interval / 60;

	if ($interval < 90)
	{ return round($interval) . ' ' . TRANS[-9]; }  // minutes

	$interval = $interval / 60;

	if ($interval < 72)
	{ return round($interval) . ' ' . TRANS[-10]; }  // hours

	return round($interval / 24) . ' ' . TRANS[-11];  // days
}