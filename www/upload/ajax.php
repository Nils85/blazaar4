<?php
spl_autoload_register(function($class_name) {
	require '../' . str_replace('\\', '/', $class_name) . '.php';
});

$link = 'No file uploaded';

if (isset($_FILES['up']))  // Upload
{
	// Avoid a browser timeout with big upload
	if (ob_get_length())
	{
		ob_flush();
		flush();
	}

	if (is_array($_FILES['up']['tmp_name']))
	{
		$filename = $_FILES['up']['name'][0];
		$path = $_FILES['up']['tmp_name'][0];
	}
	else
	{
		$filename = $_FILES['up']['name'];
		$path = $_FILES['up']['tmp_name'];
	}

	$deduplicator = Blazar\Blazar::getDeduplicator();
	$link = $deduplicator->putFile($path, $filename);

	if (empty($link))
	{ $link = 'Crappy file'; }
}

header('Content-Type: text/plain');
echo $link['ID'], $link['Code'], $link['Name'];