	</div></div>
	<div><div>
		<a href="/about/"><?php echo TRANS[6] ?></a>
		<a href="/help/"><?php echo TRANS[4] ?></a>
		<a href="https://bitbucket.org/Nils85/blazar4/issues/new" target="blank"><?php echo TRANS[5] ?></a>
		<form method="get">
			<select name="bg" onchange="selectUrl(this)">
				<option><?php echo TRANS[74] ?></option>
				<option value="1">red</option>
				<option value="2">blue</option>
				<option value="3">dark</option>
			</select>
			<noscript><input type="submit" value="change"></noscript>
		</form>
		<form method="get">
			<select name="lang" onchange="selectUrl(this)">
				<option><?php echo TRANS[1] ?></option>
				<?php
					foreach ($languages as $key => $value)
					{ echo '<option value="' . $key . '">' . $value . '</option>'; }
				?>
			</select>
			<noscript><input type="submit" value="change"></noscript>
		</form>
	</div></div>
</body>
</html>